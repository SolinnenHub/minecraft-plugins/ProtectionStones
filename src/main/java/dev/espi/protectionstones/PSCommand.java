/*
 * Copyright 2019 ProtectionStones team and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.espi.protectionstones;

import dev.espi.protectionstones.commands.*;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PSCommand extends Command {

    PSCommand(String name) {
        super(name);
    }

    static void addDefaultArguments() {
        ProtectionStones.getInstance().addCommandArgument(new ArgAddRemove());
        ProtectionStones.getInstance().addCommandArgument(new ArgAdmin());
        ProtectionStones.getInstance().addCommandArgument(new ArgBuySell());
        ProtectionStones.getInstance().addCommandArgument(new ArgCount());
        ProtectionStones.getInstance().addCommandArgument(new ArgFlag());
        ProtectionStones.getInstance().addCommandArgument(new ArgGet());
        ProtectionStones.getInstance().addCommandArgument(new ArgGive());
        ProtectionStones.getInstance().addCommandArgument(new ArgHideUnhide());
        ProtectionStones.getInstance().addCommandArgument(new ArgHome());
        ProtectionStones.getInstance().addCommandArgument(new ArgInfo());
        ProtectionStones.getInstance().addCommandArgument(new ArgList());
        ProtectionStones.getInstance().addCommandArgument(new ArgMerge());
        ProtectionStones.getInstance().addCommandArgument(new ArgName());
        ProtectionStones.getInstance().addCommandArgument(new ArgPriority());
        ProtectionStones.getInstance().addCommandArgument(new ArgRegion());
        ProtectionStones.getInstance().addCommandArgument(new ArgReload());
        ProtectionStones.getInstance().addCommandArgument(new ArgRent());
        ProtectionStones.getInstance().addCommandArgument(new ArgSethome());
        ProtectionStones.getInstance().addCommandArgument(new ArgSetparent());
        ProtectionStones.getInstance().addCommandArgument(new ArgToggle());
        ProtectionStones.getInstance().addCommandArgument(new ArgTp());
        ProtectionStones.getInstance().addCommandArgument(new ArgUnclaim());
        ProtectionStones.getInstance().addCommandArgument(new ArgView());
        ProtectionStones.getInstance().addCommandArgument(new ArgHelp());
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) {
        if (args.length == 1) {
            List<String> l = new ArrayList<>();
            for (PSCommandArg ps : ProtectionStones.getInstance().getCommandArguments()) {
                boolean hasPerm = false;
                if (ps.getPermissionsToExecute() == null) {
                    hasPerm = true;
                } else {
                    for (String perm : ps.getPermissionsToExecute()) {
                        if (sender.hasPermission(perm)) {
                            hasPerm = true;
                            break;
                        }
                    }
                }
                if (hasPerm) l.addAll(ps.getNames());
            }
            return StringUtil.copyPartialMatches(args[0], l, new ArrayList<>());
        } else if (args.length >= 2) {
            for (PSCommandArg ps : ProtectionStones.getInstance().getCommandArguments()) {
                for (String arg : ps.getNames()) {
                    if (arg.equalsIgnoreCase(args[0])) {
                        return ps.tabComplete(sender, alias, args);
                    }
                }
            }
        }
        return null;
    }

    @Override
    public boolean execute(CommandSender s, String label, String[] args) {
        if (args.length == 0) { // no arguments
            if (s instanceof ConsoleCommandSender) {
                s.sendMessage(ChatColor.RED + "You can only use /ps reload, /ps admin, /ps give from console.");
            } else {
                new ArgHelp().executeArgument(s, args, null);
            }
            return true;
        }
        for (PSCommandArg command : ProtectionStones.getInstance().getCommandArguments()) {
            if (command.getNames().contains(args[0])) {
                if (command.allowNonPlayersToExecute() || s instanceof Player) {

                    // extract flags
                    List<String> nArgs = new ArrayList<>();
                    HashMap<String, String> flags = new HashMap<>();
                    for (int i = 0; i < args.length; i++) {

                        if (command.getRegisteredFlags() != null && command.getRegisteredFlags().containsKey(args[i])) {
                            if (command.getRegisteredFlags().get(args[i])) { // has value after
                                if (i != args.length-1) flags.put(args[i], args[++i]);
                            } else {
                                flags.put(args[i], null);
                            }
                        } else {
                            nArgs.add(args[i]);
                        }
                    }

                    return command.executeArgument(s, nArgs.toArray(new String[0]), flags);
                } else if (!command.allowNonPlayersToExecute()) {
                    s.sendMessage(ChatColor.RED + "You can only use /ps reload, /ps admin, /ps give from console.");
                    return true;
                }
            }
        }

        PSL.msg(s, PSL.NO_SUCH_COMMAND.msg());
        return true;
    }
}
